create table roles
(
    role_id    varchar(1020)            not null,
    org_id     varchar(255)             not null,
    app_id     varchar(255)             not null,
    name       varchar(520)             not null,
    children   varchar(255)[]           not null,
    tags       varchar(255)[]           not null,
    created_on timestamp with time zone not null,
    updated_on timestamp with time zone not null,
    constraint roles1_pk
        primary key (org_id, app_id, role_id),
    constraint roles_owner__fk
        foreign key (org_id, app_id) references apps (org_id, app_id)
);

comment on table roles is 'Roles';

comment on column roles.role_id is 'Identity of a role';

comment on column roles.app_id is 'Application owning the role';

comment on column roles.org_id is 'Organisation owning the role';

comment on column roles.children is 'Name of the child role';

comment on column roles.name is 'Name of the role';

comment on column roles.tags is 'List of tags';

comment on column roles.created_on is 'Date on which created';

comment on column roles.updated_on is 'Date on which updated';

