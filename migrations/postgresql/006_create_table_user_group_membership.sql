create table users_group_membership
(
    org_id     varchar(255)                           not null,
    group_id   varchar(255)                           not null,
    user_id    varchar(255)                           not null,
    created_on timestamp with time zone default now() not null,
    constraint users_group_membership_pk
        primary key (org_id, group_id, user_id),
    constraint users_group_membership_groups_id_owner_org_fk
        foreign key (group_id, org_id) references groups,
    constraint users_group_membership_users_member_org_id_fk
        foreign key (org_id, user_id) references users (member_org, id)
);

comment on table users_group_membership is 'Contains membership of a User in a Group';

comment on column users_group_membership.org_id is 'Organization to which the User and Group belong to';

comment on column users_group_membership.group_id is 'Identity of the Group the user is member of.';

comment on column users_group_membership.user_id is 'Identity of the User';

comment on column users_group_membership.created_on is 'Date on which the membership was created';

create index users_group_membership_org_id_group_id_index
    on users_group_membership (org_id, group_id);

create index users_group_membership_org_id_user_id_index
    on users_group_membership (org_id, user_id);

