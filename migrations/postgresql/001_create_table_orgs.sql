create table public.orgs
(
    org_id     varchar(255)             not null
        constraint orgs_pk
            primary key,
    name       varchar(510)             not null,
    created_on timestamp with time zone not null,
    updated_on timestamp with time zone,
    status     varchar(255)             not null
);

comment on table public.orgs is 'Organizations ';

comment on column public.orgs.org_id is 'Identity of the Organization';

comment on column public.orgs.name is 'Name of the organization';

comment on column public.orgs.created_on is 'The date on which the organization was registed.';

comment on column public.orgs.updated_on is 'The date on which the record was last updated';

comment on column public.orgs.status is 'The status of the organization. ';

alter table public.orgs
    owner to postgres;

create unique index orgs_id_uindex
    on public.orgs (org_id);
