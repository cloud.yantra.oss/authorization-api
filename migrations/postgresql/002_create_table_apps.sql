create table public.apps
(
    org_id     varchar(255)             not null
        constraint app_owned_by_org_fk
            references public.orgs
            on delete cascade,
    app_id     varchar(255)             not null,
    name       varchar(255)             not null,
    tags       varchar(255)[]           not null,
    created_on timestamp with time zone not null,
    updated_on timestamp with time zone not null,
    constraint apps_pk
        primary key (org_id, app_id)
);

comment on table public.apps is 'Applications';

comment on column public.apps.org_id is 'Organization owning the application';

comment on column public.apps.app_id is 'Identity of an application ';

comment on column public.apps.name is 'Name of the application';

comment on constraint app_owned_by_org_fk on public.apps is 'Application owned by organization .';

comment on column public.apps.tags is 'Tags on application';

comment on column public.apps.created_on is 'Date on which the app was created';

comment on column public.apps.updated_on is 'Date on which the app was aupdated';

alter table public.apps
    owner to postgres;

create index apps_id_index
    on public.apps (app_id);

create index apps_owned_by_org_index
    on public.apps (org_id);
