create table users
(
    user_id    varchar(1020)            not null,
    org_id     varchar(255)             not null,
    app_id     varchar(255)             not null,
    name       varchar(520)             not null,
    groups     varchar(255)[]           not null,
    roles      varchar(255)[]           not null,
    tags       varchar(255)[]           not null,
    created_on timestamp with time zone not null,
    updated_on timestamp with time zone not null,
    constraint users1_pk
        primary key (org_id, app_id, user_id),
    constraint users_owner__fk
        foreign key (org_id, app_id) references apps (org_id, app_id)
);

comment on table users is 'Users';

comment on column users.user_id is 'Identity of user';

comment on column users.org_id is 'The identity of the organization, the user is member of';
comment on column users.app_id is 'The identity of the application, the user is member of';

comment on column users.name is 'Name of the user';

comment on column users.groups is 'Groups the user is member of';

comment on column users.roles is 'Roles the user is assigned';

comment on column users.tags is 'Tags on the user';

comment on column users.created_on is 'The date the record for user was created';

comment on column users.updated_on is 'The date on which the user record was upated';


create unique index users_id_member_org_uindex
    on users (user_id, org_id, app_id);

create index users_member_org_index
    on users (org_id);

create index users_member_org_and_app_index
    on users (org_id, app_id);
