create table public.groups
(
    id           varchar(255)                                         not null,
    owner_org    varchar(255)                                         not null
        constraint groups_orgs_id_fk
            references public.orgs,
    name         varchar(510)                                         not null,
    tags         varchar(255)[] default ARRAY []::character varying[] not null,
    created_on   timestamp with time zone                             not null,
    updated_on   varchar(255)                                         not null,
    c_attributes jsonb          default '{}'::jsonb                   not null,
    constraint groups_pk
        primary key (id, owner_org)
);

comment on table public.groups is 'Group of Users';

comment on column public.groups.id is 'Identity of the group';

comment on column public.groups.owner_org is 'Organization owning the Group';

comment on column public.groups.name is 'Name of the group';

comment on column public.groups.tags is 'Tags';

comment on column public.groups.created_on is 'Date on which this record was created';

comment on column public.groups.updated_on is 'Date on which the record was updated';

comment on column public.groups.c_attributes is 'A custom attrubute ';

alter table public.groups
    owner to postgres;

create index groups_owner_org_index
    on public.groups (owner_org);
