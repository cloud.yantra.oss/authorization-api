create table group_roles_assignment
(
    org_id    varchar(255)             not null,
    group_id  varchar(255)             not null,
    app_id    varchar(255)             not null,
    role_id   varchar(255)             not null,
    create_on timestamp with time zone not null,
    constraint group_roles_assignment_pk
        primary key (org_id, group_id, app_id, role_id),
    constraint group_roles_assignment_groups_id_owner_org_fk
        foreign key (group_id, org_id) references groups,
    constraint group_roles_assignment_roles_owner_org_id_fk
        foreign key (org_id, app_id, role_id) references roles (owner_org, owner_app, id)
);

comment on table group_roles_assignment is 'Roles assigned to a Group';

comment on column group_roles_assignment.org_id is 'Organization Id owning this assignment of Role';

comment on column group_roles_assignment.group_id is 'Identity of the Group to which the Role is assigned';

comment on column group_roles_assignment.app_id is 'Identity of the App to which the Group is assigned, and the Role belongs to';

comment on column group_roles_assignment.role_id is 'Identity of the Role assigned to Group';

comment on column group_roles_assignment.create_on is 'The date on which the Role was assigned to Group';

create index group_roles_assignment_org_id_group_id_index
    on group_roles_assignment (org_id, app_id, group_id);

create index group_roles_assignment_org_id_role_id_index
    on group_roles_assignment (org_id, app_id, role_id);
