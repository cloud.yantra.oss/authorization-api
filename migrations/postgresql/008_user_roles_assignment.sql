create table user_roles_assignment
(
    org_id    varchar(255)             not null,
    user_id   varchar(255)             not null,
    app_id    varchar(255)             not null,
    role_id   varchar(255)             not null,
    create_on timestamp with time zone not null,
    constraint user_roles_assignment_pk
        primary key (org_id, user_id, app_id, role_id),
    constraint user_roles_assignment_users_id_owner_org_fk
        foreign key (user_id, org_id) references users,
    constraint user_roles_assignment_roles_owner_org_id_fk
        foreign key (org_id, app_id, role_id) references roles (owner_org, owner_app, id)
);

comment on table user_roles_assignment is 'Roles assigned to a User';

comment on column user_roles_assignment.org_id is 'Organization Id owning this assignment of Role';

comment on column user_roles_assignment.user_id is 'Identity of the User to which the Role is assigned';

comment on column user_roles_assignment.app_id is 'Identity of the App to which the User is assigned, and the Role belongs to';

comment on column user_roles_assignment.role_id is 'Identity of the Role assigned to User';

comment on column user_roles_assignment.create_on is 'The date on which the Role was assigned to User';

create index user_roles_assignment_org_id_user_id_index
    on user_roles_assignment (org_id, app_id, user_id);

create index user_roles_assignment_org_id_role_id_index
    on user_roles_assignment (org_id, app_id, role_id);
