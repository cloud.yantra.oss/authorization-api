use crate::models::domain::orgs::OrgId;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};
use uuid::Uuid;

#[derive(Debug, Clone, Eq, PartialEq, Hash, Deserialize, Serialize)]
pub struct AppId(pub String);

impl Default for AppId {
    fn default() -> Self {
        Self(Uuid::new_v4().to_string())
    }
}

impl Display for AppId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct App {
    pub id: AppId,
    pub org_id: OrgId,
    pub name: Option<String>,
    pub tags: Vec<String>,
    pub created_on: DateTime<Utc>,
    pub updated_on: DateTime<Utc>,
}
