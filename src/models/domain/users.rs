use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};
use uuid::Uuid;

use crate::models::domain::apps::AppId;
use crate::models::domain::groups::GroupId;
use crate::models::domain::orgs::OrgId;
use crate::models::domain::roles::RoleId;

#[derive(Debug, Clone, Eq, PartialEq, Hash, Deserialize, Serialize)]
pub struct UserId(pub String);

impl Default for UserId {
    fn default() -> Self {
        Self(Uuid::new_v4().to_string())
    }
}

impl Display for UserId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct User {
    pub id: UserId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub name: String,
    pub groups: Vec<GroupId>,
    pub roles: Vec<RoleId>,
    pub tags: Vec<String>,
    pub created_on: DateTime<Utc>,
    pub updated_on: DateTime<Utc>,
}
