use crate::models::domain::apps::AppId;
use crate::models::domain::orgs::OrgId;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::models::domain::roles::RoleId;

#[derive(Debug, Deserialize, Serialize, Clone, Eq, PartialEq, Hash)]
pub struct GroupId(pub String);

impl GroupId {
    pub fn new(id: String) -> Self {
        Self(id)
    }
}

impl Default for GroupId {
    fn default() -> Self {
        Self(Uuid::new_v4().to_string())
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Group {
    pub id: GroupId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub name: String,
    pub children: Vec<GroupId>,
    pub roles: Vec<RoleId>,
    pub tags: Vec<String>,
    pub created_on: DateTime<Utc>,
    pub updated_on: DateTime<Utc>,
}
