use std::fmt::{Display, Formatter};

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

//
#[derive(Debug, Clone, Eq, PartialEq, Hash, Deserialize, Serialize)]
pub struct OrgId(pub String);

impl Default for OrgId {
    fn default() -> Self {
        Self(Uuid::new_v4().to_string())
    }
}

impl Display for OrgId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", &self.0)
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Deserialize, Serialize)]
pub enum Status {
    #[serde(rename = "active")]
    Active,
    #[serde(rename = "dormant")]
    Dormant,
    #[serde(rename = "undefined")]
    Undefined,
}

impl Status {
    pub fn as_str(&self) -> &'static str {
        match *self {
            Status::Active => "a",
            Status::Dormant => "d",
            _ => "u",
        }
    }

    pub fn from_str(s: &str) -> Status {
        match s.to_lowercase().as_str() {
            "a" => Status::Active,
            "d" => Status::Dormant,
            _ => Status::Undefined,
        }
    }
}

impl Default for Status {
    fn default() -> Self {
        Status::Dormant
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Org {
    pub id: OrgId,
    pub name: String,
    pub status: Status,
    pub created_on: DateTime<Utc>,
    pub updated_on: DateTime<Utc>,
}

// #[derive(Debug, Clone, Eq, PartialEq, Hash, Deserialize, Serialize)]
// pub struct OrgAttributes {
//     pub name: String,
//     pub status: Status,
//     pub created_on: DateTime<Utc>,
//     pub updated_on: DateTime<Utc>,
// }
//
// #[derive(Serialize)]
// pub struct OrgJson {
//     id: OrgId,
//     #[serde(rename = "best.type")]
//     type_: String,
//     attributes: OrgAttributes,
// }
//
// impl From<Org> for OrgJson {
//     fn from(org: Org) -> Self {
//         Self {
//             id: org.id,
//             type_: "org".to_string(),
//             attributes: OrgAttributes {
//                 name: org.name,
//                 status: org.status,
//                 created_on: org.created_on,
//                 updated_on: org.updated_on,
//             },
//         }
//     }
// }
