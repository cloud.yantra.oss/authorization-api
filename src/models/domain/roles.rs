use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::models::domain::apps::AppId;
use crate::models::domain::orgs::OrgId;

#[derive(Debug, Clone, Eq, PartialEq, Hash, Deserialize, Serialize)]
pub struct RoleId(pub String);

impl RoleId {
    pub fn new(id: String) -> Self {
        Self(id)
    }
}

impl Default for RoleId {
    fn default() -> Self {
        Self(Uuid::new_v4().to_string())
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Role {
    pub id: RoleId,
    pub name: Option<String>,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub children: Vec<RoleId>,
    pub tags: Vec<String>,
    pub created_on: DateTime<Utc>,
    pub updated_on: DateTime<Utc>,
}
