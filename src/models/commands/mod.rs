use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};
use uuid::Uuid;

pub mod apps;
pub mod orgs;
pub mod roles;
pub mod users;

pub mod groups;

#[derive(Debug, Clone, Eq, PartialEq, Hash, Deserialize, Serialize)]
pub struct CommandId(pub Uuid);

impl CommandId {
    pub fn new(uuid: Uuid) -> Self {
        Self(uuid)
    }
}

impl Default for CommandId {
    fn default() -> Self {
        CommandId(Uuid::new_v4())
    }
}

impl Display for CommandId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", &self.0)
    }
}
