use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::models::commands::CommandId;
use crate::models::domain::orgs::{OrgId, Status};

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterNewOrg {
    pub id: CommandId,
    pub org_id: OrgId,
    pub name: String,
    pub status: Status,
}

impl RegisterNewOrg {
    pub fn new_basic(org_id: OrgId) -> Self {
        Self {
            org_id: org_id.clone(),
            name: org_id.0,
            ..Default::default()
        }
    }

    pub fn new_random_org() -> Self {
        Default::default()
    }
}

impl Default for RegisterNewOrg {
    fn default() -> Self {
        let random_id = Uuid::new_v4().to_string();
        Self {
            id: Default::default(),
            org_id: OrgId(random_id.clone()),
            name: random_id.clone(),
            status: Status::Dormant,
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct DeleteOrg {
    pub id: CommandId,
    pub org_id: OrgId,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct UpdateName {
    pub id: CommandId,
    pub org_id: OrgId,
    pub name: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Update {
    pub id: CommandId,
    pub org_id: OrgId,
    pub name: Option<String>,
    pub status: Option<Status>,
}
