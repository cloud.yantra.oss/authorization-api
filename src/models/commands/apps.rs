use crate::models::commands::CommandId;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::models::domain::apps::AppId;
use crate::models::domain::orgs::OrgId;

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterNewApp {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub name: String,
    pub tags: Vec<String>,
}

impl RegisterNewApp {
    pub fn new_basic(org_id: OrgId, app_id: AppId) -> Self {
        Self {
            org_id,
            app_id: app_id.clone(),
            name: app_id.0,
            ..Default::default()
        }
    }

    pub fn new_random_app(org_id: OrgId) -> Self {
        Self {
            org_id,
            ..Default::default()
        }
    }
}

impl Default for RegisterNewApp {
    fn default() -> Self {
        let random_id = Uuid::new_v4().to_string();
        RegisterNewApp {
            id: Default::default(),
            org_id: OrgId(random_id.clone()),
            app_id: AppId(random_id.clone()),
            name: random_id.clone(),
            tags: vec![],
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct DeleteApp {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
}

impl Default for DeleteApp {
    fn default() -> Self {
        let random_id = Uuid::new_v4().to_string();
        DeleteApp {
            id: Default::default(),
            org_id: OrgId(random_id.clone()),
            app_id: AppId(random_id.clone()),
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Update {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub name: Option<String>,
    pub tags: Option<Vec<String>>,
}
