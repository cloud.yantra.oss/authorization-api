use crate::models::commands::CommandId;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::models::domain::apps::AppId;
use crate::models::domain::groups::GroupId;
use crate::models::domain::orgs::OrgId;
use crate::models::domain::roles::RoleId;

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterNewGroup {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub group_id: GroupId,
    pub name: String,
    pub children: Vec<GroupId>,
    pub roles: Vec<RoleId>,
    pub tags: Vec<String>,
}

impl Default for RegisterNewGroup {
    fn default() -> Self {
        let random_id = Uuid::new_v4().to_string();
        Self {
            id: Default::default(),
            org_id: Default::default(),
            app_id: Default::default(),
            group_id: GroupId(random_id.clone()),
            name: random_id,
            children: vec![],
            roles: vec![],
            tags: vec![],
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct DeleteGroup {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub group_id: GroupId,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct UpdateName {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub group_id: GroupId,
    pub name: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct AssignRole {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub group_id: GroupId,
    pub role_id: RoleId,
}
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct DetachRole {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub group_id: GroupId,
    pub role_id: RoleId,
}
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct AttachChildGroup {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub group_id: GroupId,
    pub child_group_id: GroupId,
}
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct DetachChildGroup {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub group_id: GroupId,
    pub child_group_id: GroupId,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AddTag {
    pub id: Option<CommandId>,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub group_id: GroupId,
    pub tag: String,
}
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DeleteTag {
    pub id: Option<CommandId>,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub group_id: GroupId,
    pub tag: String,
}
