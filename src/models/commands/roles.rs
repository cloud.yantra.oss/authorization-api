use serde::{Deserialize, Serialize};

use crate::models::commands::CommandId;
use crate::models::domain::apps::AppId;
use crate::models::domain::orgs::OrgId;
use crate::models::domain::roles::RoleId;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct RegisterNewRole {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub role_id: RoleId,
    pub name: String,
    pub children: Vec<RoleId>,
    pub tags: Vec<String>,
}

impl RegisterNewRole {
    pub fn new_basic(org_id: OrgId, app_id: AppId, role_id: RoleId) -> Self {
        Self {
            org_id,
            app_id,
            role_id: role_id.clone(),
            name: role_id.0,
            ..Default::default()
        }
    }
    pub fn new_random_role(org_id: OrgId, app_id: AppId) -> Self {
        Self {
            org_id,
            app_id,
            ..Default::default()
        }
    }
}

impl Default for RegisterNewRole {
    fn default() -> Self {
        let random_role_id: RoleId = Default::default();
        Self {
            id: Default::default(),
            org_id: Default::default(),
            app_id: Default::default(),
            role_id: random_role_id.clone(),
            name: random_role_id.0,
            children: vec![],
            tags: vec![],
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DeleteRole {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub role_id: RoleId,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AttachChildRole {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub role_id: RoleId,
    pub child_role_id: RoleId,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DetachChildRole {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub role_id: RoleId,
    pub child_role_id: RoleId,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AddTag {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub role_id: RoleId,
    pub tag: String,
}
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DeleteTag {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub role_id: RoleId,
    pub tag: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct UpdateName {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub role_id: RoleId,
    pub name: String,
}
