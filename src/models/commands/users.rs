use crate::models::commands::CommandId;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::models::domain::apps::AppId;
use crate::models::domain::groups::GroupId;
use crate::models::domain::orgs::OrgId;
use crate::models::domain::roles::RoleId;
use crate::models::domain::users::UserId;

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterNewUser {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub user_id: UserId,
    pub name: String,
    pub groups: Vec<GroupId>,
    pub roles: Vec<RoleId>,
    pub tags: Vec<String>,
}

impl Default for RegisterNewUser {
    fn default() -> Self {
        let random_id = Uuid::new_v4().to_string();
        Self {
            id: Default::default(),
            org_id: Default::default(),
            app_id: Default::default(),
            user_id: UserId(random_id.clone()),
            name: random_id,
            groups: vec![],
            roles: vec![],
            tags: vec![],
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct DeleteUser {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub user_id: UserId,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct UpdateName {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub user_id: UserId,
    pub name: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct AssignRole {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub user_id: UserId,
    pub role_id: RoleId,
}
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct DetachRole {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub user_id: UserId,
    pub role_id: RoleId,
}
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct AssignGroup {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub user_id: UserId,
    pub group_id: GroupId,
}
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct DetachGroup {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub user_id: UserId,
    pub group_id: GroupId,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AddTag {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub user_id: UserId,
    pub tag: String,
}
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DeleteTag {
    pub id: CommandId,
    pub org_id: OrgId,
    pub app_id: AppId,
    pub user_id: UserId,
    pub tag: String,
}
