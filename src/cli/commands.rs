use clap::{value_parser, Arg, ArgMatches, Command};

use crate::api;
use crate::cli::configs::AppConfiguration;
use crate::repos::postgresql::store::Store;

pub fn configure() -> Command {
    Command::new("Authorization API Service")
        .subcommand(migrate())
        .subcommand(start())
}

fn migrate() -> Command {
    Command::new("migrate")
        .about("Execute the migration scripts")
        .long_flag("migrate")
        .short_flag('m')
        .arg(
            Arg::new("database")
                .long("database")
                .short('d')
                .default_value("postgres"),
        )
}

fn start() -> Command {
    Command::new("start")
        .about("Start the authorization API service.")
        .long_flag("start")
        .short_flag('s')
        .arg(
            Arg::new("address")
                .value_parser(value_parser!(String))
                .long("address")
                .short('a'),
        )
        .arg(
            Arg::new("port")
                // Do not allow the OS reserved ports
                .value_parser(value_parser!(u16).range(1024..))
                .long("port")
                .short('p'),
        )
}

pub async fn handle(matches: &ArgMatches, config: &AppConfiguration) -> anyhow::Result<()> {
    handle_migrate(matches, config)?;
    handle_start(matches, config).await?;
    Ok(())
}

fn handle_migrate(matches: &ArgMatches, config: &AppConfiguration) -> anyhow::Result<()> {
    match matches.subcommand_matches("migrate") {
        None => {}
        Some(_) => {
            let store = Box::new(Store::new(&config.database.postgresql));
            tracing::info!("Executing scripts for migration.")
        }
    }
    Ok(())
}

async fn handle_start(matches: &ArgMatches, config: &AppConfiguration) -> anyhow::Result<()> {
    match matches.subcommand_matches("start") {
        None => Ok(()),
        Some(m) => {
            let store = Box::new(Store::new(&config.database.postgresql));
            let address = m.get_one::<String>("address").map(|s| s.to_string());
            let port = m.get_one::<u16>("port").map(|p| *p);
            let address: String = address.unwrap_or(config.serve.address.clone());
            let port: u16 = port.unwrap_or(config.serve.port);
            api::start_server(&store, &address, port).await
        }
    }
}
