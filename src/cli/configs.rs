use anyhow::Result;
use config::{Config, Environment, File, Source};
use serde::Deserialize;

use crate::repos;

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct Database {
    pub postgresql: repos::postgresql::store::Configuration,
}

impl Default for Database {
    fn default() -> Self {
        Database {
            postgresql: default_postgres_configuration(),
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct Logging {
    pub log_level: String,
}
impl Default for Logging {
    fn default() -> Self {
        Logging {
            log_level: "info".to_string(),
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct Hosting {
    pub address: String,
    pub port: u16,
}

impl Default for Hosting {
    fn default() -> Self {
        Hosting {
            address: "127.0.0.1".to_string(),
            port: 3000,
        }
    }
}

#[derive(Debug, Deserialize, Clone, Default)]
#[allow(unused)]
pub struct AppConfiguration {
    #[serde(default)]
    pub database: Database,
    #[serde(default)]
    pub logging: Logging,
    #[serde(default)]
    pub serve: Hosting,
}

impl AppConfiguration {
    pub fn new(
        file_path: &str,
        env_prefix: &str,
        env_prefix_separator: &str,
        env_field_separator: &str,
    ) -> Result<Self> {
        let c = Config::builder()
            .add_source(File::with_name(file_path))
            .add_source(
                Environment::with_prefix(env_prefix)
                    .prefix_separator(env_prefix_separator)
                    .separator(env_field_separator),
            )
            .build()?;

        c.collect()?
            .iter()
            .for_each(|(k, v)| tracing::debug!("Config key: {} === value: {}", k, v));

        let c = c.try_deserialize()?;
        Ok(c)
    }
}

fn default_postgres_configuration() -> repos::postgresql::store::Configuration {
    repos::postgresql::store::Configuration {
        username: "admin".to_string(),
        password: "password".to_string(),
        database: "authz".to_string(),
        host: "localhost".to_string(),
        port: 5432,
        max_connections: None,
    }
}

#[test]
fn test_app_configuration_parsing() -> Result<()> {
    let default_file_path = "configs/default.yaml";
    let default_env_prefix = "app";

    let source = Environment::default().source(Some({
        let mut env = std::collections::HashMap::new();
        env.insert("APP_DATABASE_POSTGRESQL_DATABASE".into(), "db1".into());
        env.insert("APP_DATABASE_POSTGRESQL_USERNAME".into(), "admin".into());
        env.insert("APP_DATABASE_POSTGRESQL_PASSWORD".into(), "passw0rd".into());
        env.insert("APP_DATABASE_POSTGRESQL_HOST".into(), "192.168.1.1".into());
        env.insert("APP_DATABASE_POSTGRESQL_PORT".into(), "1234".into());
        env
    }));
    let source = source
        .prefix(default_env_prefix)
        .prefix_separator("_")
        .separator("_");

    let c = Config::builder()
        .add_source(File::with_name(default_file_path))
        .add_source(source)
        .build()?;

    c.collect()?
        .iter()
        .for_each(|(k, v)| println!("Config key: {} === value: {}", k, v));

    let config = c.try_deserialize::<AppConfiguration>();
    match config {
        Ok(config) => {
            assert_eq!(config.database.postgresql.username, "admin");
            assert_eq!(config.database.postgresql.database, "db1");
            assert_eq!(config.database.postgresql.password, "passw0rd");
            assert_eq!(config.database.postgresql.host, "192.168.1.1");
            assert_eq!(config.database.postgresql.port, 1234);
        }
        Err(e) => {
            println!("The configuration parsing failed due to: {:?}", e);
            assert!(false);
        }
    }

    Ok(())
}
