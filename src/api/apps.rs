use std::str::FromStr;

use axum::extract::{Path, State};
use axum::http::StatusCode;
use axum::response::Json;
use axum::routing::{delete, get, patch, post};
use axum::Router;
use jsonapi_rs::jsonapi_model;
use jsonapi_rs::model::*;
use serde::{Deserialize, Serialize};

use crate::models::commands::CommandId;
use crate::models::domain::apps::{App, AppId};
use crate::models::domain::orgs::OrgId;
use crate::repos::postgresql::store::Store;
use crate::repos::repos::AppRepo;

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[allow(unused)]
struct NewApp {
    #[serde(default)]
    pub id: CommandId,
    pub app_id: AppId,
    pub name: String,
    pub tags: Vec<String>,
}
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[allow(unused)]
struct PatchApp {
    #[serde(default)]
    pub id: CommandId,
    pub name: Option<String>,
    pub tags: Option<Vec<String>>,
}

jsonapi_model!(NewApp; "newApp");
jsonapi_model!(PatchApp; "updateApp");
jsonapi_model!(App; "app");

pub fn routes(store: Box<Store>) -> Router {
    let routes = Router::new()
        .route("/orgs/:org_id/apps/:app_id", get(get_app))
        .route("/orgs/:org_id/apps/:app_id", delete(delete_app))
        .route("/orgs/:org_id/apps", post(register_app))
        .route("/orgs/:org_id/apps/:app_id", patch(update_app));
    routes.with_state(store)
}

async fn get_app(
    State(store): State<Box<Store>>,
    Path((org_id, app_id)): Path<(OrgId, AppId)>,
) -> (StatusCode, Json<Option<JsonApiDocument>>) {
    let app = store.get_app_by_id(&org_id, &app_id).await;
    match app {
        Ok(a) => (StatusCode::OK, Json(Some(a.to_jsonapi_document()))),
        Err(e) => {
            tracing::error!(
                "Fetching application id {} for organization {} failed. Error -> {}",
                app_id,
                org_id,
                e
            );
            (StatusCode::NOT_FOUND, Json(None))
        }
    }
}

async fn delete_app(
    State(store): State<Box<Store>>,
    Path((org_id, app_id)): Path<(OrgId, AppId)>,
) -> (StatusCode, Json<Option<JsonApiDocument>>) {
    let cmd = crate::models::commands::apps::DeleteApp {
        org_id,
        app_id,
        ..Default::default()
    };
    let app = store.delete_app(&cmd).await;
    match app {
        Ok(a) => (StatusCode::OK, Json(Some(a.to_jsonapi_document()))),
        Err(_) => (StatusCode::NOT_FOUND, Json(None)),
    }
}

async fn register_app(
    State(store): State<Box<Store>>,
    Path(org_id): Path<OrgId>,
    cmd: String,
) -> (StatusCode, Json<Option<JsonApiDocument>>) {
    let payload = JsonApiDocument::from_str(&cmd);
    match payload {
        Ok(JsonApiDocument::Data(data)) => {
            let cmd = NewApp::from_jsonapi_document(&data).map(|c| {
                crate::models::commands::apps::RegisterNewApp {
                    id: c.id,
                    org_id,
                    app_id: c.app_id,
                    name: c.name,
                    tags: c.tags,
                }
            });
            match cmd {
                Ok(c) => {
                    let app = store.register_new_app(&c).await;
                    match app {
                        Ok(a) => (StatusCode::CREATED, Json(Some(a.to_jsonapi_document()))),
                        Err(_) => (StatusCode::NOT_FOUND, Json(None)),
                    }
                }
                Err(_) => (StatusCode::BAD_REQUEST, Json(None)),
            }
        }
        _ => (StatusCode::BAD_REQUEST, Json(None)),
    }
}

async fn update_app(
    State(store): State<Box<Store>>,
    Path((org_id, app_id)): Path<(OrgId, AppId)>,
    cmd: String,
) -> (StatusCode, Json<Option<JsonApiDocument>>) {
    let payload = JsonApiDocument::from_str(&cmd);
    match payload {
        Ok(JsonApiDocument::Data(data)) => {
            let cmd = PatchApp::from_jsonapi_document(&data).map(|c| {
                crate::models::commands::apps::Update {
                    id: c.id,
                    org_id,
                    app_id,
                    name: c.name,
                    tags: c.tags,
                }
            });
            match cmd {
                Ok(c) => {
                    let app = store.update(&c).await;
                    match app {
                        Ok(a) => (StatusCode::OK, Json(Some(a.to_jsonapi_document()))),
                        Err(e) => (StatusCode::NOT_FOUND, Json(None)),
                    }
                }
                Err(_) => (StatusCode::BAD_REQUEST, Json(None)),
            }
        }
        _ => (StatusCode::BAD_REQUEST, Json(None)),
    }
}
