use axum::http::StatusCode;
use axum::routing::get;
use axum::Router;
use tower_http::trace::TraceLayer;

use crate::repos::postgresql::store::Store;

pub mod apps;
pub mod errors;
pub mod orgs;
pub mod roles;
pub mod users;

pub async fn start_server(store: &Box<Store>, address: &str, port: u16) -> anyhow::Result<()> {
    let healthz: Router = Router::new().route("/healthz", get(|| async { StatusCode::OK }));
    let routes = orgs::routes(store.clone())
        .merge(apps::routes(store.clone()))
        .merge(roles::routes(store.clone()))
        .merge(healthz)
        .layer(TraceLayer::new_for_http());

    let full_address = format!("{}:{}", address, port);
    tracing::info!("Starting the Authorization Service at {}:{}", address, port);
    let listener = tokio::net::TcpListener::bind(full_address).await.unwrap();
    axum::serve(listener, routes).await.unwrap();
    Ok(())
}
