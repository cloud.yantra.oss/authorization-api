use std::str::FromStr;

use axum::extract::{Path, State};
use axum::http::StatusCode;
use axum::response::Json;
use axum::routing::{delete, get, patch, post};
use axum::Router;
use jsonapi_rs::jsonapi_model;
use jsonapi_rs::model::JsonApiModel;
use jsonapi_rs::model::*;
use serde::{Deserialize, Serialize};

use crate::models::commands::orgs::{DeleteOrg, RegisterNewOrg, Update};
use crate::models::commands::CommandId;
use crate::models::domain::orgs::{Org, OrgId, Status};
use crate::repos::postgresql::store::Store;
use crate::repos::repos::OrgRepo;

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct NewOrg {
    #[serde(default)]
    pub id: CommandId,
    pub org_id: OrgId,
    pub name: String,
    #[serde(default)]
    pub status: Status,
}
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[allow(unused)]
struct PatchOrg {
    #[serde(default)]
    pub id: CommandId,
    pub name: Option<String>,
    pub status: Option<Status>,
}

jsonapi_model!(NewOrg;"newOrg");
jsonapi_model!(PatchOrg; "updateOrg");
jsonapi_model!(Org; "org");

pub fn routes(store: Box<Store>) -> Router {
    let routes = Router::new()
        // .route("/orgs", post(post_org))
        .route("/orgs/:org_id", get(get_org))
        .route("/orgs/:org_id", delete(delete_org))
        .route("/orgs/:org_id", patch(update_org))
        .route("/orgs", post(register_org));
    routes.with_state(store)
}

async fn get_org(
    State(store): State<Box<Store>>,
    Path(org_id): Path<OrgId>,
) -> (StatusCode, Json<Option<JsonApiDocument>>) {
    let org = store.get_org_by_id(&org_id).await;
    match org {
        Ok(o) => (StatusCode::OK, Json(Some(o.to_jsonapi_document()))),
        Err(e) => {
            tracing::error!("Fetching origination by id failed. Error-> {}", e);
            (StatusCode::NOT_FOUND, Json(None))
        }
    }
}

async fn delete_org(
    State(store): State<Box<Store>>,
    Path(org_id): Path<OrgId>,
) -> (StatusCode, Json<Option<JsonApiDocument>>) {
    let cmd = DeleteOrg {
        id: CommandId::default(),
        org_id,
    };
    let org = store.delete_org(&cmd).await;
    match org {
        Ok(o) => (StatusCode::OK, Json(Some(o.to_jsonapi_document()))),
        Err(e) => {
            tracing::error!("Fetching origination by id failed. Error-> {}", e);
            (StatusCode::NOT_FOUND, Json(None))
        }
    }
}

async fn register_org(
    State(store): State<Box<Store>>,
    cmd: String,
) -> (StatusCode, Json<Option<JsonApiDocument>>) {
    let payload = JsonApiDocument::from_str(&cmd);
    match payload {
        Ok(JsonApiDocument::Data(data)) => {
            let cmd = NewOrg::from_jsonapi_document(&data).map(|c| RegisterNewOrg {
                id: c.id,
                org_id: c.org_id,
                name: c.name,
                status: c.status,
            });
            match cmd {
                Ok(cmd) => {
                    let org = store.register_new_org(&cmd).await;
                    match org {
                        Ok(o) => (StatusCode::CREATED, Json(Some(o.to_jsonapi_document()))),
                        Err(_) => (StatusCode::BAD_REQUEST, Json(None)),
                    }
                }
                _ => (StatusCode::BAD_REQUEST, Json(None)),
            }
        }
        _ => (StatusCode::BAD_REQUEST, Json(None)),
    }
}

async fn update_org(
    State(store): State<Box<Store>>,
    Path(org_id): Path<OrgId>,
    cmd: String,
) -> (StatusCode, Json<Option<JsonApiDocument>>) {
    let payload = JsonApiDocument::from_str(&cmd);
    match payload {
        Ok(JsonApiDocument::Data(data)) => {
            let cmd = PatchOrg::from_jsonapi_document(&data).map(|c| Update {
                id: c.id,
                org_id,
                name: c.name,
                status: c.status,
            });
            match cmd {
                Ok(cmd) => {
                    let org = store.update(&cmd).await;
                    match org {
                        Ok(o) => (StatusCode::OK, Json(Some(o.to_jsonapi_document()))),
                        Err(_) => (StatusCode::BAD_REQUEST, Json(None)),
                    }
                }
                _ => (StatusCode::BAD_REQUEST, Json(None)),
            }
        }
        _ => (StatusCode::BAD_REQUEST, Json(None)),
    }
}
