use std::fmt::{Debug, Display, Formatter};

// pub type  AppErrorResponse = (StatusCode,String );

pub struct InvalidInput {
    details: String,
}

impl InvalidInput {
    // pub(crate) fn new(msg: &str) -> InvalidInput {
    //     InvalidInput {
    //         details: msg.to_string()
    //     }
    // }
}

impl Debug for InvalidInput {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Display for InvalidInput {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl std::error::Error for InvalidInput {}
