use axum::extract::{Path, State};
use axum::http::StatusCode;
use axum::routing::post;
use axum::{Json, Router};

use crate::models::commands::roles::{AttachChildRole, RegisterNewRole};
use crate::models::domain::apps::AppId;
use crate::models::domain::roles::{Role, RoleId};
use crate::repos::postgresql::store::Store;
use crate::repos::repos::RoleRepo;

pub fn routes(store: Box<Store>) -> Router {
    Router::new()
        .route("/apps/:app_id/roles", post(post_roles))
        .route(
            "/apps/:app_id/roles/:role_id/children",
            post(post_roles_children),
        )
        .with_state(store)
}

async fn post_roles(
    State(store): State<Box<Store>>,
    Path(_app_id): Path<String>,
    Json(cmd): Json<RegisterNewRole>,
) -> (StatusCode, Json<Option<Role>>) {
    let result = store.register_new_role(&cmd).await;
    match result {
        Ok(role) => (StatusCode::CREATED, Json(Some(role))),
        Err(e) => {
            tracing::error!(
                "Role could not be created with command [{:?}]. Error ==> {}",
                &cmd,
                e
            );
            (StatusCode::BAD_REQUEST, Json(None))
        }
    }
}

async fn post_roles_children(
    State(store): State<Box<Store>>,
    Path((app_id, role_id)): Path<(AppId, RoleId)>,
    Json(cmd): Json<AttachChildRole>,
) -> (StatusCode, Json<Option<Role>>) {
    let result = store.attach_child_role(&cmd).await;
    match result {
        Ok(role) => (StatusCode::OK, Json(Some(role))),
        Err(e) => {
            tracing::error!(
                "Child Role [{:?}] could not be added to Role[{:?}] for App[{:?}]. Error ==> {}",
                &cmd.role_id,
                role_id,
                app_id,
                e
            );
            (StatusCode::BAD_REQUEST, Json(None))
        }
    }
}
