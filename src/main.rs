use tracing::Level;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::Registry;

use crate::cli::configs::AppConfiguration;

mod api;
mod cli;
mod models;
mod repos;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    dotenv::dotenv().ok();
    let subscriber = Registry::default()
        .with(tracing::metadata::LevelFilter::from_level(Level::DEBUG))
        .with(tracing_subscriber::fmt::Layer::default().with_writer(std::io::stdout));
    tracing::subscriber::set_global_default(subscriber).expect("Failed to set tracing subscriber.");

    let config = fetch_configurations();
    let cli_commands = cli::commands::configure();
    let matches = cli_commands.get_matches();
    cli::commands::handle(&matches, &config).await?;

    Ok(())
}

fn fetch_configurations() -> AppConfiguration {
    // TODO: The defaults should be pushed to commands
    let default_file_path = "configs/default.yaml";
    let default_env_prefix = "app";
    let default_env_prefix_separator = "_";
    let default_env_field_separator = "_";
    let parsed_configuration = AppConfiguration::new(
        default_file_path,
        default_env_prefix,
        default_env_prefix_separator,
        default_env_field_separator,
    );
    let config = match parsed_configuration {
        Ok(c) => c,
        Err(e) => {
            panic!("Failure in loading Configurations. Error ==> {}", e);
        }
    };
    tracing::debug!("Configurations:\n {:?} ", config);
    config
}
