use anyhow::{Context, Result};
use serde::Deserialize;
use sqlx::postgres::{PgPoolOptions, PgRow};
use sqlx::PgPool;
use sqlx::Row;

use crate::models::commands::apps::{DeleteApp, RegisterNewApp};
use crate::models::commands::groups::{
    AssignRole, AttachChildGroup, DeleteGroup, DetachChildGroup, DetachRole, RegisterNewGroup,
};
use crate::models::commands::orgs::{DeleteOrg, RegisterNewOrg, Update};
use crate::models::commands::roles::{
    AddTag, AttachChildRole, DeleteRole, DeleteTag, DetachChildRole, RegisterNewRole,
};
use crate::models::commands::users::{AssignGroup, DeleteUser, DetachGroup, RegisterNewUser};
use crate::models::domain::apps::{App, AppId};
use crate::models::domain::groups::{Group, GroupId};
use crate::models::domain::orgs::{Org, OrgId, Status};
use crate::models::domain::roles::{Role, RoleId};
use crate::models::domain::users::{User, UserId};
use crate::repos::repos::{AppRepo, GroupRepo, OrgRepo, RoleRepo, UserRepo};

#[derive(Clone)]
pub struct Store {
    pub connection: PgPool,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Configuration {
    pub username: String,
    pub password: String,
    pub database: String,
    pub host: String,
    pub port: u32,
    pub max_connections: Option<u32>,
}

impl Store {
    pub fn new(config: &Configuration) -> Self {
        let db_url = format!(
            "postgresql://{}:{}@{}:{}/{}",
            config.username, config.password, config.host, config.port, config.database
        );

        let pool = match PgPoolOptions::new()
            .max_connections(config.max_connections.unwrap_or(5))
            .connect_lazy(&db_url)
        {
            Ok(pool) => pool,
            Err(e) => panic!("Could not establish migrations connection. Error ==> {}", e),
        };

        Store { connection: pool }
    }
}

impl OrgRepo for Store {
    async fn register_new_org(&self, cmd: &RegisterNewOrg) -> Result<Org> {
        let result = sqlx::query(
            r#"            
            INSERT into orgs (org_id, name, status, created_on, updated_on)
            VALUES ($1, $2, $3, $4, $5)
            RETURNING org_id, name, status, created_on, updated_on
    "#,
        )
        .bind(&cmd.org_id.0)
        .bind(&cmd.name)
        .bind(&cmd.status.as_str())
        .bind(chrono::Utc::now())
        .bind(chrono::Utc::now())
        .map(|r| extract_org(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not register an organization with provided data: {:?}",
        //         &cmd
        //     )
        // })?;
        Ok(result)
    }

    async fn delete_org(&self, cmd: &DeleteOrg) -> anyhow::Result<Org> {
        let result = sqlx::query(
            r#"            
            DELETE from orgs where org_id = $1
            RETURNING org_id, name, status, created_on, updated_on
    "#,
        )
        .bind(&cmd.org_id.0)
        .map(|r| extract_org(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not delete Org with provided data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn update(&self, cmd: &Update) -> anyhow::Result<Org> {
        let result = sqlx::query(
            r#"                                    
            UPDATE orgs
            SET name = COALESCE($1, name), status = COALESCE($2, status), updated_on = now()
            WHERE org_id = $3
            RETURNING org_id, name, status, created_on, updated_on
    "#,
        )
        .bind(&cmd.name)
        .bind(&cmd.status.clone().map(|s| s.as_str().to_string()))
        .bind(&cmd.org_id.0)
        .map(|r| extract_org(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not update the status for Org with provided data: {:?}",
        //         &cmd
        //     )
        // })?;
        Ok(result)
    }

    // async fn update_name(&self, cmd: &UpdateName) -> anyhow::Result<Org> {
    //     let result = sqlx::query(
    //         r#"
    //     UPDATE orgs
    //     SET name = $1, updated_on = now()
    //     WHERE org_id = $2
    //     RETURNING org_id, name, status, created_on, updated_on
    // "#,
    //     )
    //     .bind(&cmd.name)
    //     .bind(&cmd.org_id.0)
    //     .map(|r| extract_org(r))
    //     .fetch_one(&self.connection)
    //     .await?;
    //     // .with_context(|| {
    //     //     format!(
    //     //         "Could not update name for Org with provided data: {:?}",
    //     //         &cmd
    //     //     )
    //     // })?;
    //     Ok(result)
    // }

    async fn get_org_by_id(&self, id: &OrgId) -> Result<Org> {
        let result = sqlx::query(r#"SELECT * from orgs where org_id = $1"#)
            .bind(&id.0)
            // .bind(&id)
            .map(|r| extract_org(r))
            .fetch_one(&self.connection)
            .await?;
        // .with_context(|| format!("Could not find organization with id {:?}", &id))?;
        Ok(result)
    }
}

impl AppRepo for Store {
    async fn register_new_app(&self, cmd: &RegisterNewApp) -> Result<App> {
        tracing::debug!("Registering application using command: {:?}", &cmd);
        let result = sqlx::query(
            r#"
        INSERT into apps (org_id, app_id, name, tags, created_on, updated_on)
        VALUES ($1, $2, $3, $4, $5, $6)
        RETURNING org_id, app_id, name, tags, created_on, updated_on
        "#,
        )
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.name)
        .bind(&cmd.tags)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(sqlx::types::chrono::Utc::now())
        .map(|r| extract_app(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could register a new app with provided data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn delete_app(&self, cmd: &DeleteApp) -> Result<App> {
        tracing::debug!("Deleting application using command: {:?}", &cmd);
        let result = sqlx::query(
            r#"
        delete from apps where org_id = $1 and app_id = $2
        RETURNING org_id, app_id, name, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .map(|r| extract_app(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not delete application with provided data: {:?}",
        //         &cmd
        //     )
        // })?;
        Ok(result)
    }

    async fn update(&self, cmd: &crate::models::commands::apps::Update) -> Result<App> {
        tracing::debug!("Updating application using command: {:?}", &cmd);
        let result = sqlx::query(
            r#"            
            UPDATE apps
            SET name = COALESCE($1, name), tags = COALESCE($2, tags) , updated_on = $3
            WHERE org_id = $4 and app_id = $5
            RETURNING org_id, app_id, name, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.name)
        .bind(&cmd.tags)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .map(|r| extract_app(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not update the application's name with provided data: {:?}",
        //         &cmd
        //     )
        // })?;
        Ok(result)
    }

    async fn get_app_by_id(&self, org_id: &OrgId, app_id: &AppId) -> Result<App> {
        tracing::debug!("Fetching application using app Id: {:?}", app_id);
        let result = sqlx::query(r#"SELECT * from apps where org_id = $1 and  app_id = $2 "#)
            .bind(&org_id.0)
            .bind(&app_id.0)
            .map(|r| extract_app(r))
            .fetch_one(&self.connection)
            .await
            .with_context(|| {
                format!(
                    "Could not find an App with id {:?} for organization with id {:?}",
                    &app_id, &org_id
                )
            })?;
        Ok(result)
    }

    async fn fetch_all_apps_for_org(&self, org_id: &OrgId) -> Result<Vec<App>> {
        let result = sqlx::query(r#"SELECT * from apps where org_id = $1"#)
            .bind(&org_id.0)
            .map(|r| extract_app(r))
            .fetch_all(&self.connection)
            .await?;
        // .with_context(|| {
        //     format!("Could not find apps for organization with id {:?}", &org_id)
        // })?;
        Ok(result)
    }
}

impl RoleRepo for Store {
    async fn register_new_role(&self, cmd: &RegisterNewRole) -> Result<Role> {
        let result = sqlx::query(
            r#"
        INSERT INTO roles ( org_id, app_id, role_id, name, children, tags, created_on, updated_on)
        VALUES ( $1, $2, $3, $4, $5, $6 ,$7, $8)
        RETURNING org_id, app_id, role_id, name, children, tags, created_on, updated_on
        "#,
        )
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.role_id.0)
        .bind(&cmd.name)
        .bind(
            &cmd.children
                .iter()
                .map(|r| r.0.clone())
                .collect::<Vec<String>>(),
        )
        .bind(&cmd.tags)
        .bind(chrono::Utc::now())
        .bind(chrono::Utc::now())
        .map(|r| extract_role(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not register Role with provided data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn delete_role(&self, cmd: &DeleteRole) -> Result<Role> {
        let result = sqlx::query(
            r#"
            DELETE from roles 
            WHERE org_id = $1 and app_id = $2 and role_id = $3
            RETURNING org_id, app_id, role_id, name, children, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.role_id.0)
        .map(|r| extract_role(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not delete role with provided data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn update_name(&self, cmd: &crate::models::commands::roles::UpdateName) -> Result<Role> {
        let result = sqlx::query(
            r#"
            UPDATE roles
            SET name = $1 , updated_on = $2
            WHERE org_id = $3 and app_id = $4 and role_id = $5
            RETURNING org_id, app_id, role_id, name, children, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.name)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.role_id.0)
        .map(|r| extract_role(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not update the Role's name with provided data: {:?}",
        //         &cmd
        //     )
        // })?;
        Ok(result)
    }

    async fn attach_child_role(&self, cmd: &AttachChildRole) -> Result<Role> {
        let result = sqlx::query(
            r#"
        UPDATE roles
        SET children = array_append(children,$1), updated_on = $2
        WHERE org_id = $3 and app_id = $4 AND role_id = $5
        AND NOT ($6 = ANY (children))
        RETURNING org_id, app_id, role_id, name, children, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.child_role_id.0)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.role_id.0)
        .bind(&cmd.child_role_id.0)
        .map(|r| extract_role(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not include a Role as Child role using data: {:?}",
        //         &cmd
        //     )
        // })?;
        Ok(result)
    }

    async fn detach_child_role(&self, cmd: &DetachChildRole) -> Result<Role> {
        let result = sqlx::query(
            r#"            
            UPDATE roles
            SET children = array_remove(children,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND role_id = $5
            AND $6 = ANY (children)
            RETURNING org_id, app_id, role_id, name, children, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.child_role_id.0)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.role_id.0)
        .bind(&cmd.child_role_id.0)
        .map(|r| extract_role(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not remove a Role as Child role using data: {:?}",
        //         &cmd
        //     )
        // })?;
        Ok(result)
    }

    async fn add_tag(&self, cmd: &AddTag) -> Result<Role> {
        let result = sqlx::query(
            r#"
            UPDATE roles
            SET tags = array_append(tags,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND role_id = $5
            AND NOT ($6 = ANY (tags))
            RETURNING org_id, app_id, role_id, name, children, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.tag)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.role_id.0)
        .bind(&cmd.tag)
        .map(|r| extract_role(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not include a Tag for Role using data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn delete_tag(&self, cmd: &DeleteTag) -> Result<Role> {
        let result = sqlx::query(
            r#"
            UPDATE roles
            SET tags = array_remove(tags,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND role_id = $5 AND $6 = ANY (tags)
            RETURNING org_id, app_id, role_id, name, children, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.tag)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.role_id.0)
        .bind(&cmd.tag)
        .map(|r| extract_role(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not remove a Tag from Role using data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn get_role_by_id(
        &self,
        org_id: &OrgId,
        app_id: &AppId,
        role_id: RoleId,
    ) -> Result<Role> {
        let result = sqlx::query(
            r#"SELECT * from roles where org_id = $1 and  app_id = $2 and role_id = $3"#,
        )
        .bind(&org_id.0)
        .bind(&app_id.0)
        .bind(&role_id.0)
        .map(|r| extract_role(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not find an Role {:?} for App with id {:?} and Organization with id {:?}",
        //         &role_id, &app_id, &org_id
        //     )
        // })?;
        Ok(result)
    }

    async fn fetch_all_roles_for_app(&self, org_id: &OrgId, app_id: &AppId) -> Result<Vec<Role>> {
        let result = sqlx::query(r#"SELECT * from roles where org_id = $1 and  app_id = $2"#)
            .bind(&org_id.0)
            .bind(&app_id.0)
            .map(|r| extract_role(r))
            .fetch_all(&self.connection)
            .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not find any role for App with id {:?} and Organization with id {:?}",
        //         &app_id, &org_id
        //     )
        // })?;
        Ok(result)
    }

    async fn fetch_all_roles_for_org(&self, org_id: &OrgId) -> Result<Vec<Role>> {
        let result = sqlx::query(r#"SELECT * from roles where org_id = $1"#)
            .bind(&org_id.0)
            .map(|r| extract_role(r))
            .fetch_all(&self.connection)
            .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not find any role for organization with id {:?}",
        //         &org_id
        //     )
        // })?;
        Ok(result)
    }
}

impl UserRepo for Store {
    async fn register_new_user(&self, cmd: &RegisterNewUser) -> Result<User> {
        let result = sqlx::query(
            r#"
            INSERT INTO users ( org_id, app_id, user_id, name, groups, roles, tags, created_on, updated_on)
            VALUES ( $1, $2, $3, $4, $5, $6 ,$7, $8, $9)
            RETURNING org_id, app_id, user_id, name, groups, roles, tags, created_on, updated_on
        "#,
        )
            .bind(&cmd.org_id.0)
            .bind(&cmd.app_id.0)
            .bind(&cmd.user_id.0)
            .bind(&cmd.name)
            .bind(
                &cmd.groups
                    .iter()
                    .map(|r| r.0.clone())
                    .collect::<Vec<String>>(),
            )
            .bind(
                &cmd.roles
                    .iter()
                    .map(|r| r.0.clone())
                    .collect::<Vec<String>>(),
            )
            .bind(&cmd.tags)
            .bind(chrono::Utc::now())
            .bind(chrono::Utc::now())
            .map(|r| extract_user(r))
            .fetch_one(&self.connection)
            .await?;
        // .with_context(|| format!("Could not register User with provided data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn delete_user(&self, cmd: &DeleteUser) -> Result<User> {
        let result = sqlx::query(
            r#"
            DELETE from users
            WHERE org_id = $1 and app_id = $2 and user_id = $3
            RETURNING org_id, app_id, user_id, name, groups, roles, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.user_id.0)
        .map(|r| extract_user(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not delete User with provided data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn update_name(&self, cmd: &crate::models::commands::users::UpdateName) -> Result<User> {
        let result = sqlx::query(
            r#"
            UPDATE users
            SET name = $1 , updated_on = $2
            WHERE org_id = $3 and app_id = $4 and user_id = $5
            RETURNING org_id, app_id, user_id, name, groups, roles, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.name)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.user_id.0)
        .map(|r| extract_user(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not update the User's name with provided data: {:?}",
        //         &cmd
        //     )
        // })?;
        Ok(result)
    }

    async fn assign_role(&self, cmd: &crate::models::commands::users::AssignRole) -> Result<User> {
        let result = sqlx::query(
            r#"
            UPDATE users
            SET roles = array_append(roles,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND user_id = $5
            AND NOT ($6 = ANY (roles))
            RETURNING org_id, app_id, user_id, name, groups, roles, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.role_id.0)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.user_id.0)
        .bind(&cmd.role_id.0)
        .map(|r| extract_user(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not include assign a Role to User using data: {:?}",
        //         &cmd
        //     )
        // })?;
        Ok(result)
    }

    async fn detach_role(&self, cmd: &crate::models::commands::users::DetachRole) -> Result<User> {
        let result = sqlx::query(
            r#"
            UPDATE users
            SET roles = array_remove(roles,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND user_id = $5 AND $6 = ANY (roles)
            RETURNING org_id, app_id, user_id, name, groups, roles, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.role_id.0)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.user_id.0)
        .bind(&cmd.role_id.0)
        .map(|r| extract_user(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not detach a Role from User using data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn assign_group(&self, cmd: &AssignGroup) -> Result<User> {
        let result = sqlx::query(
            r#"
            UPDATE users
            SET groups = array_append(groups,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND user_id = $5
            AND NOT ($6 = ANY (groups))
            RETURNING org_id, app_id, user_id, name, groups, roles, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.group_id.0)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.user_id.0)
        .bind(&cmd.group_id.0)
        .map(|r| extract_user(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not include a User in a Group using data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn detach_group(&self, cmd: &DetachGroup) -> Result<User> {
        let result = sqlx::query(
            r#"
            UPDATE users
            SET groups = array_remove(groups,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND user_id = $5 AND $6 = ANY (groups)
            RETURNING org_id, app_id, user_id, name, groups, roles, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.group_id.0)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.user_id.0)
        .bind(&cmd.group_id.0)
        .map(|r| extract_user(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not remove a User from Group using data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn add_tag(&self, cmd: &crate::models::commands::users::AddTag) -> Result<User> {
        let result = sqlx::query(
            r#"
            UPDATE users
            SET tags = array_append(tags,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND user_id = $5
            AND NOT ($6 = ANY (tags))
            RETURNING org_id, app_id, user_id, name, groups, roles, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.tag)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.user_id.0)
        .bind(&cmd.tag)
        .map(|r| extract_user(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not add a Tag for User using data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn delete_tag(&self, cmd: &crate::models::commands::users::DeleteTag) -> Result<User> {
        let result = sqlx::query(
            r#"
            UPDATE users
            SET tags = array_remove(tags,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND user_id = $5 AND $6 = ANY (tags)
            RETURNING org_id, app_id, user_id, name, groups, roles, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.tag)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.user_id.0)
        .bind(&cmd.tag)
        .map(|r| extract_user(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not remove a Tag from User using data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn get_user_by_id(
        &self,
        org_id: &OrgId,
        app_id: &AppId,
        user_id: UserId,
    ) -> Result<User> {
        let result = sqlx::query(
            r#"SELECT * from users where org_id = $1 and  app_id = $2 and user_id = $3"#,
        )
        .bind(&org_id.0)
        .bind(&app_id.0)
        .bind(&user_id.0)
        .map(|r| extract_user(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not find an User {:?} for App with id {:?} and Organization with id {:?}",
        //         &user_id, &app_id, &org_id
        //     )
        // })?;
        Ok(result)
    }

    async fn fetch_all_users_for_app(&self, org_id: &OrgId, app_id: &AppId) -> Result<Vec<User>> {
        let result = sqlx::query(r#"SELECT * from users where org_id = $1 and  app_id = $2"#)
            .bind(&org_id.0)
            .bind(&app_id.0)
            .map(|r| extract_user(r))
            .fetch_all(&self.connection)
            .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not find Users for App with id {:?} and Organization with id {:?}",
        //         &app_id, &org_id
        //     )
        // })?;
        Ok(result)
    }

    async fn fetch_all_users_for_org(&self, org_id: &OrgId) -> Result<Vec<User>> {
        let result = sqlx::query(r"SELECT * from users where org_id = $1")
            .bind(&org_id.0)
            .map(|r| extract_user(r))
            .fetch_all(&self.connection)
            .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not find Users for Organization with id {:?}",
        //         &org_id
        //     )
        // })?;
        Ok(result)
    }
}

impl GroupRepo for Store {
    async fn register_new_group(&self, cmd: &RegisterNewGroup) -> Result<Group> {
        let result = sqlx::query(
            r#"
            INSERT INTO groups ( org_id, app_id, group_id, name, children, roles, tags, created_on, updated_on)
            VALUES ( $1, $2, $3, $4, $5, $6 ,$7, $8, $9)
            RETURNING org_id, app_id, group_id, name, children, roles, tags, created_on, updated_on
        "#,
        )
            .bind(&cmd.org_id.0)
            .bind(&cmd.app_id.0)
            .bind(&cmd.group_id.0)
            .bind(&cmd.name)
            .bind(
                &cmd.children
                    .iter()
                    .map(|r| r.0.clone())
                    .collect::<Vec<String>>(),
            )
            .bind(
                &cmd.roles
                    .iter()
                    .map(|r| r.0.clone())
                    .collect::<Vec<String>>(),
            )
            .bind(&cmd.tags)
            .bind(chrono::Utc::now())
            .bind(chrono::Utc::now())
            .map(|r| extract_group(r))
            .fetch_one(&self.connection)
            .await?;
        // .with_context(|| format!("Could not register Group with provided data: {:?}", &cmd))?;
        Ok(result)
    }

    /// Todo: On deleting a group, it should also be removed from User's assigned group.
    async fn delete_group(&self, cmd: &DeleteGroup) -> Result<Group> {
        let result = sqlx::query(
            r#"
            DELETE from groups
            WHERE org_id = $1 and app_id = $2 and group_id = $3
            RETURNING org_id, app_id, group_id, name, children, roles, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.group_id.0)
        .map(|r| extract_group(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not delete Group with provided data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn update_name(
        &self,
        cmd: &crate::models::commands::groups::UpdateName,
    ) -> Result<Group> {
        let result = sqlx::query(
            r#"
            UPDATE groups
            SET name = $1 , updated_on = $2
            WHERE org_id = $3 and app_id = $4 and group_id = $5
            RETURNING org_id, app_id, group_id, name, children, roles, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.name)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.group_id.0)
        .map(|r| extract_group(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not update the Group's name with provided data: {:?}",
        //         &cmd
        //     )
        // })?;
        Ok(result)
    }

    async fn assign_role(&self, cmd: &AssignRole) -> Result<Group> {
        let result = sqlx::query(
            r#"
            UPDATE groups
            SET roles = array_append(roles,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND group_id = $5
            AND NOT ($6 = ANY (roles))
            RETURNING org_id, app_id, group_id, name, children, roles, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.role_id.0)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.group_id.0)
        .bind(&cmd.role_id.0)
        .map(|r| extract_group(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not include assign a Role to Group using data: {:?}",
        //         &cmd
        //     )
        // })?;
        Ok(result)
    }

    async fn detach_role(&self, cmd: &DetachRole) -> Result<Group> {
        let result = sqlx::query(
            r#"
            UPDATE groups
            SET roles = array_remove(roles,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND group_id = $5 AND $6 = ANY (roles)
            RETURNING org_id, app_id, group_id, name, children, roles, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.role_id.0)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.group_id.0)
        .bind(&cmd.role_id.0)
        .map(|r| extract_group(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not detach a Role from Group using data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn attach_child_group(&self, cmd: &AttachChildGroup) -> Result<Group> {
        let result = sqlx::query(
            r#"
        UPDATE groups
        SET children = array_append(children,$1), updated_on = $2
        WHERE org_id = $3 and app_id = $4 AND group_id = $5
        AND NOT ($6 = ANY (children))
        RETURNING org_id, app_id, group_id, name, children, roles, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.child_group_id.0)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.group_id.0)
        .bind(&cmd.child_group_id.0)
        .map(|r| extract_group(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not include a Group as Child group using data: {:?}",
        //         &cmd
        //     )
        // })?;
        Ok(result)
    }

    async fn detach_child_group(&self, cmd: &DetachChildGroup) -> Result<Group> {
        let result = sqlx::query(
            r#"            
            UPDATE groups
            SET children = array_remove(children,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND group_id = $5
            AND $6 = ANY (children)
            RETURNING org_id, app_id, group_id, name, children, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.child_group_id.0)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.group_id.0)
        .bind(&cmd.child_group_id.0)
        .map(|r| extract_group(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not remove a Group as Child group using data: {:?}",
        //         &cmd
        //     )
        // })?;
        Ok(result)
    }

    async fn add_tag(&self, cmd: &crate::models::commands::groups::AddTag) -> Result<Group> {
        let result = sqlx::query(
            r#"
            UPDATE groups
            SET tags = array_append(tags,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND group_id = $5
            AND NOT ($6 = ANY (tags))
            RETURNING org_id, app_id, group_id, name, children, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.tag)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.group_id.0)
        .bind(&cmd.tag)
        .map(|r| extract_group(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not include a Tag for Group using data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn delete_tag(&self, cmd: &crate::models::commands::groups::DeleteTag) -> Result<Group> {
        let result = sqlx::query(
            r#"
            UPDATE groups
            SET tags = array_remove(tags,$1), updated_on = $2
            WHERE org_id = $3 and app_id = $4 AND group_id = $5 AND $6 = ANY (tags)
            RETURNING org_id, app_id, group_id, name, children, tags, created_on, updated_on
    "#,
        )
        .bind(&cmd.tag)
        .bind(sqlx::types::chrono::Utc::now())
        .bind(&cmd.org_id.0)
        .bind(&cmd.app_id.0)
        .bind(&cmd.group_id.0)
        .bind(&cmd.tag)
        .map(|r| extract_group(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| format!("Could not remove a Tag from Group using data: {:?}", &cmd))?;
        Ok(result)
    }

    async fn get_group_by_id(
        &self,
        org_id: &OrgId,
        app_id: &AppId,
        group_id: GroupId,
    ) -> Result<Group> {
        let result = sqlx::query(
            r#"SELECT * from groups where org_id = $1 and  app_id = $2 and group_id = $3"#,
        )
        .bind(&org_id.0)
        .bind(&app_id.0)
        .bind(&group_id.0)
        .map(|r| extract_group(r))
        .fetch_one(&self.connection)
        .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not find an Group {:?} for App with id {:?} and Organization with id {:?}",
        //         &group_id, &app_id, &org_id
        //     )
        // })?;
        Ok(result)
    }

    async fn fetch_all_groups_for_app(&self, org_id: &OrgId, app_id: &AppId) -> Result<Vec<Group>> {
        let result = sqlx::query(r#"SELECT * from groups where org_id = $1 and  app_id = $2"#)
            .bind(&org_id.0)
            .bind(&app_id.0)
            .map(|r| extract_group(r))
            .fetch_all(&self.connection)
            .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not find Groups for App with id {:?} and Organization with id {:?}",
        //         &app_id, &org_id
        //     )
        // })?;
        Ok(result)
    }

    async fn fetch_all_groups_for_org(&self, org_id: &OrgId) -> Result<Vec<Group>> {
        let result = sqlx::query(r#"SELECT * from groups where org_id = $1"#)
            .bind(&org_id.0)
            .map(|r| extract_group(r))
            .fetch_all(&self.connection)
            .await?;
        // .with_context(|| {
        //     format!(
        //         "Could not find Groups for Organization with id {:?}",
        //         &org_id
        //     )
        // })?;
        Ok(result)
    }
}

fn extract_org(row: PgRow) -> Org {
    Org {
        id: OrgId(row.get("org_id")),
        name: row.get("name"),
        status: Status::from_str(row.get("status")),
        created_on: row.get("created_on"),
        updated_on: row.get("updated_on"),
    }
}

fn extract_app(row: PgRow) -> App {
    App {
        id: AppId(row.get("app_id")),
        org_id: OrgId(row.get("org_id")),
        name: row.get("name"),
        tags: row.get("tags"),
        created_on: row.get("created_on"),
        updated_on: row.get("updated_on"),
    }
}

fn extract_role(row: PgRow) -> Role {
    let children = transform_array_to_domain_ids(&row, "children", RoleId::new);
    Role {
        id: RoleId(row.get("role_id")),
        org_id: OrgId(row.get("org_id")),
        app_id: AppId(row.get("app_id")),
        name: row.get("name"),
        children,
        tags: row.get("tags"),
        created_on: row.get("created_on"),
        updated_on: row.get("updated_on"),
    }
}

fn extract_user(row: PgRow) -> User {
    let groups = transform_array_to_domain_ids(&row, "groups", GroupId::new);
    let roles = transform_array_to_domain_ids(&row, "roles", RoleId::new);
    User {
        id: UserId(row.get("user_id")),
        org_id: OrgId(row.get("org_id")),
        app_id: AppId(row.get("app_id")),
        name: row.get("name"),
        groups,
        roles,
        tags: row.get("tags"),
        created_on: row.get("created_on"),
        updated_on: row.get("updated_on"),
    }
}

fn extract_group(row: PgRow) -> Group {
    let children = transform_array_to_domain_ids(&row, "children", GroupId::new);
    let roles = transform_array_to_domain_ids(&row, "roles", RoleId::new);
    Group {
        id: GroupId(row.get("group_id")),
        org_id: OrgId(row.get("org_id")),
        app_id: AppId(row.get("app_id")),
        name: row.get("name"),
        children,
        roles,
        tags: row.get("tags"),
        created_on: row.get("created_on"),
        updated_on: row.get("updated_on"),
    }
}

/// A helper function to iterate over an Array , and return a Vector of Doimain Type
fn transform_array_to_domain_ids<DomainId>(
    row: &PgRow,
    column_name: &str,
    f: fn(String) -> DomainId,
) -> Vec<DomainId> {
    row.get::<Vec<String>, &str>(column_name)
        .iter()
        .map(|r| f(r.clone()))
        .collect::<Vec<DomainId>>()
}
