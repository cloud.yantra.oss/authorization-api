use anyhow::Result;

use crate::models::commands::apps::{DeleteApp, RegisterNewApp};
use crate::models::commands::groups::{
    AttachChildGroup, DeleteGroup, DetachChildGroup, RegisterNewGroup,
};
use crate::models::commands::orgs::{DeleteOrg, RegisterNewOrg};
use crate::models::commands::roles::{
    AttachChildRole, DeleteRole, DetachChildRole, RegisterNewRole,
};
use crate::models::commands::users::{AssignGroup, DeleteUser, DetachGroup, RegisterNewUser};
use crate::models::domain::apps::{App, AppId};
use crate::models::domain::groups::{Group, GroupId};
use crate::models::domain::orgs::{Org, OrgId};
use crate::models::domain::roles::{Role, RoleId};
use crate::models::domain::users::{User, UserId};

pub trait OrgRepo {
    async fn register_new_org(&self, cmd: &RegisterNewOrg) -> Result<Org>;
    async fn delete_org(&self, cmd: &DeleteOrg) -> Result<Org>;
    async fn update(&self, cmd: &crate::models::commands::orgs::Update) -> Result<Org>;
    async fn get_org_by_id(&self, id: &OrgId) -> Result<Org>;
}

pub trait AppRepo {
    async fn register_new_app(&self, cmd: &RegisterNewApp) -> Result<App>;
    async fn delete_app(&self, cmd: &DeleteApp) -> Result<App>;
    async fn update(&self, cmd: &crate::models::commands::apps::Update) -> Result<App>;
    async fn get_app_by_id(&self, org_id: &OrgId, app_id: &AppId) -> Result<App>;
    async fn fetch_all_apps_for_org(&self, org_id: &OrgId) -> Result<Vec<App>>;
}

pub trait RoleRepo {
    async fn register_new_role(&self, cmd: &RegisterNewRole) -> Result<Role>;
    async fn delete_role(&self, cmd: &DeleteRole) -> Result<Role>;
    async fn update_name(&self, cmd: &crate::models::commands::roles::UpdateName) -> Result<Role>;
    async fn attach_child_role(&self, cmd: &AttachChildRole) -> Result<Role>;
    async fn detach_child_role(&self, cmd: &DetachChildRole) -> Result<Role>;
    async fn add_tag(&self, cmd: &crate::models::commands::roles::AddTag) -> Result<Role>;
    async fn delete_tag(&self, cmd: &crate::models::commands::roles::DeleteTag) -> Result<Role>;
    async fn get_role_by_id(&self, org_id: &OrgId, app_id: &AppId, role_id: RoleId)
        -> Result<Role>;
    async fn fetch_all_roles_for_app(&self, org_id: &OrgId, app_id: &AppId) -> Result<Vec<Role>>;
    async fn fetch_all_roles_for_org(&self, org_id: &OrgId) -> Result<Vec<Role>>;
}

pub trait UserRepo {
    async fn register_new_user(&self, cmd: &RegisterNewUser) -> Result<User>;
    async fn delete_user(&self, cmd: &DeleteUser) -> Result<User>;
    async fn update_name(&self, cmd: &crate::models::commands::users::UpdateName) -> Result<User>;
    async fn assign_role(&self, cmd: &crate::models::commands::users::AssignRole) -> Result<User>;
    async fn detach_role(&self, cmd: &crate::models::commands::users::DetachRole) -> Result<User>;
    async fn assign_group(&self, cmd: &AssignGroup) -> Result<User>;
    async fn detach_group(&self, cmd: &DetachGroup) -> Result<User>;
    async fn add_tag(&self, cmd: &crate::models::commands::users::AddTag) -> Result<User>;
    async fn delete_tag(&self, cmd: &crate::models::commands::users::DeleteTag) -> Result<User>;
    async fn get_user_by_id(&self, org_id: &OrgId, app_id: &AppId, user_id: UserId)
        -> Result<User>;
    async fn fetch_all_users_for_app(&self, org_id: &OrgId, app_id: &AppId) -> Result<Vec<User>>;
    async fn fetch_all_users_for_org(&self, org_id: &OrgId) -> Result<Vec<User>>;
}

pub trait GroupRepo {
    async fn register_new_group(&self, cmd: &RegisterNewGroup) -> Result<Group>;
    async fn delete_group(&self, cmd: &DeleteGroup) -> Result<Group>;
    async fn update_name(&self, cmd: &crate::models::commands::groups::UpdateName)
        -> Result<Group>;
    async fn assign_role(&self, cmd: &crate::models::commands::groups::AssignRole)
        -> Result<Group>;
    async fn detach_role(&self, cmd: &crate::models::commands::groups::DetachRole)
        -> Result<Group>;
    async fn attach_child_group(&self, cmd: &AttachChildGroup) -> Result<Group>;
    async fn detach_child_group(&self, cmd: &DetachChildGroup) -> Result<Group>;
    async fn add_tag(&self, cmd: &crate::models::commands::groups::AddTag) -> Result<Group>;
    async fn delete_tag(&self, cmd: &crate::models::commands::groups::DeleteTag) -> Result<Group>;
    async fn get_group_by_id(
        &self,
        org_id: &OrgId,
        app_id: &AppId,
        group_id: GroupId,
    ) -> Result<Group>;
    async fn fetch_all_groups_for_app(&self, org_id: &OrgId, app_id: &AppId) -> Result<Vec<Group>>;
    async fn fetch_all_groups_for_org(&self, org_id: &OrgId) -> Result<Vec<Group>>;
}
