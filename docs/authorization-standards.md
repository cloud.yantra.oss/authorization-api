>**Version**: `v1.0`

# Authorization Service and Domain Model
> **Note:** Goal of this document is to capture and explain the Architecture and Design aspects of authorization. This documents 
> does not aim to capture project specific requirements.
> The document is focusing on coarse and fined grained authorization for any application or service hosted on MercerOS platform, primarily.
> The document borrow concepts from other systems in the industry, especially major cloud vendors. It attempts to propose a simplified but yet a flexible solution to meet complex requirements.
> 
> Pdf version of this document is available [here.](authorization-standards.pdf)


Authorization will be enabled by following:
- Authorization policy engine will be provided by [OPA(Open Policy Agent)](https://www.openpolicyagent.org/)
- Authorization control data store will be provided by the [MercerOS Authorization Service API](https://bitbucket.org/oliverwymantechssg/ngpd-merceros-authorization-client/src/master/)

Authentication is provided by [MSSO(Mercer Single SignOn)](https://wiki.mercer.com/display/MSS/Mercer+Single+Sign+On) for all 
human users and by Apigee for system users for enabling service-to-service communications.

## 1. Authorization Concepts
Authorization is the function of specifying access rights/privileges to resources, which is related to information security and computer security in general and to access control in particular. More formally, "to authorize" is to define an access policy. For example, human resources staff are normally authorized to access employee records and this policy is usually formalized as access control rules in a computer system. During operation, the system uses the access control rules to decide whether access requests from (authenticated) consumers shall be approved (granted) or disapproved (rejected).Resources include individual files or an item's data, computer programs, computer devices and functionality provided by computer applications. Examples of consumers are computer users, computer Software and other Hardware on the computer.

Most modern, multi-user operating systems include access control and thereby rely on authorization. Access control also uses authentication to verify the identity of consumers. When a consumer tries to access a resource, the access control process checks that the consumer has been authorized to use that resource. Authorization is the responsibility of an *Data Owner* or *System of Record*.

 Authorizations are expressed as access policies in some types of "policy definition application", e.g. in the form of an access control list or a capability, or a policy administration point, etc. Authorization should be based on the "*principle of the least privilege*" i.e. consumers should only be authorized to access what is neccessary for them to do their jobs.

## 2. Roles, Users, Groups, Resource and Policies

The below diagram shows the relationship between various entities in the authorization system.

```mermaid
erDiagram
Role        }|..o{ User         : assigned
Role        }|..o{ Group        : assigned
Group       }o..o{ User         : contains
Policy      }o..|{ Role         : governs
Policy      }o..|{ User         : governs
Policy      }o..|{ Group        : governs
Policy      }o..|{ Resource     : governs
```

### 2.1 Service Integration

```mermaid
graph TB
MFE(Main Front End) --> MBE(Main Bank End)
MBE --> AS(Authorization Service) & MA(Main API)
MA --- MD[(Domain Data)]
MA --> OPA(Open Policy Agent)
MA --> AS
AFE(Admin Front End) --> ABE(Admin Back End)
ABE --> AS & MA
AS --- AD[(User/Role/Permission)]
```

### 2.2 Roles

Key properties of role:
- A `Role` will contain multiple permissions. Role indicates a level of authority. 
- All roles are additive. The scope of the roles should be as small (i.e. fine) as possible. 

When roles are fine-grained, a combination of 1 or more role will usually align with a Business role.

The table below lists some example roles :

| Role Name                    | Description                                                               |
|:-----------------------------------|:--------------------------------------------------------------------------|
| app1.sys.allow.clients.abc123      | This role allows access to data of client `abc123`                        |
| app1.sys.allow.documents.create    | This role allows a user bearing this role to create document.            |

Based on the examples above, if a user needs to create a document for client `abc123`, they must have both the roles above.
The user in this case can only create the document, but they can not list or fetch a single document. To enable that they need to be also assigned following roles.


| Role Name                    | Description                                                               |
|:-----------------------------------|:--------------------------------------------------------------------------|
| app1.sys.allow.documents.read      | This role allows access to only a single document.                       |
| app1.sys.allow.documents.list      | This role allows a user to fetch and query a set of documents.            |

> Note: depending on your application, you may not have `app1.sys.allow.documents.list`. But if you deem listing to be a costly operation, you have the flexibility to create a dedicated role and control your resources.

For fine-grained rules it's good practice aligning roles with your API end points. E.g. a role to allow `GET /clients/abc123` can be represented as `app1.sys.allow.clients.abc123`.


However, if your business is really simple, then you may choose to have coarse grained roles e.g. `app1.sys.client.hr`. So any user having this role will have access to all HR related features. And if your assign role `app1.sys.allow.clients.abc123` then the user will have access to HR related functions for client `abc123`.


#### 2.2.1 Role naming conventions
When it comes to naming of the roles, all the roles will follow the below pattern:
```text
{application_key}.{generator_of_role}.{domain_role}
```

There are the segments separated by a `.` (dot). The first 2 segments are reserved segments. 

##### 2.2.1.1 `application_key`
This segment will hold the application key of the application, all in lowercase. The application key for an app on MercerOS mus be present in the wiki page : https://wiki.mercer.com/pages/viewpage.action?spaceKey=MER&title=Application+Registry.

E.g. the application key for *Broker of the Future / Intellify* is `botf`, and for *Group Benefits Portal* is `gbp`.

##### 2.2.1.2 `generator_of_role`
This segment will hold either `sys` or `usr` as value.

All roles which are created during the development process will hold a value of `sys`. All fine-grained roles will always hold a value of `sys`.

Some application may allow end users to also create roles, which would be coarse grained roles encapsulating fine-grained roles. Those roles will hold a value of `usr`. The `usr` roles will be more dymamic in nature, and `sys` role will be static in nature.  

##### 2.2.1.3 `domain_role`
Unlike the previous 2 segments, which are there to serve technical purposes, this will reflect the business domain. The application team should select a value that closely resembles the roles in their business domain.

e.g. If application key is `yaafu`, for an application named 'Yet another App for you' 
- A fine-grained role will hold a value of `allow.client.invoice.read`. Then the role will be named as `yaafu.sys.allow.client.invoice.read` 
- A coarse-grained role will hold a value of `account_manager`. Then the role will be named as `yaafu.sys.account_manager`
- A coarse-grained role created by end user allowing all rights to policies may be `manage_all-policies`. Then the role will be named as `yaafu.usr.manage_all-policies`.


### 2.3 Users
An `User` is an individual person or system that has authenticated, and is seeking access to the resource. A set of `Role` will be assigned to a user.
The MSSO provided id should be ideally used as the user id, when storing the user in the Authorization store.


### 2.4 Groups

A `Group` logically group `User` show would be performing similar roles or duties in an organization.
E.g. in case of GBP, `staff-admin-all` could be a group , which contains all admin users of GBP across geography. The roles `user-staf-admin` and `user-client-admin` are assigned to it.

#### 2.3.1 Group naming conventions
When it comes to naming of the groups, all the groups will follow the below pattern, which is similar to that of role:
```text
{application_key}.{generator_of_group}.{domain_group}
```

There are the segments separated by a `.` (dot). The first 2 segments are reserved segments. 

##### 2.3.1.1 `application_key`
This segment will hold the application key of the application, all in lowercase. The application key for an app on MercerOS must be present in the wiki page : https://wiki.mercer.com/pages/viewpage.action?spaceKey=MER&title=Application+Registry.

E.g. the application key for *Broker of the Future / Intellify* is `botf`, and for *Group Benefits Portal* is `gbp`.

##### 2.3.1.2 `generator_of_group`
This segment will hold either `sys` or `usr` as value.

All groups which are created during the development process will hold a value of `sys`.

Some application may allow end users to also create groups. These groups will hold a value of `usr`. 

##### 2.3.1.3 `domain_group`
Unlike the previous 2 segments, which are there to serve technical purposes, this will reflect the business domain. The application team should select a value that closely resembles the roles in their business domain.

e.g. If application key is `yaafu`, for an application named 'Yet another App for you' 
- A domain group will hold a value of `irish.client.hr`. Then group will be named as `yaafu.sys.irish.client.hr` 
- A domain group will hold a value of `app_admin`. Then group will be named as `yaafu.sys.app_admin`
- A domain group created by end user allowing all rights to invoice may be `invoice-management-team`. Then group will be named as `yaafu.usr.invoice-management-team`.


### 2.4 Resource
A `Resource` is any data that is served by the digital system to a requester. The examples above `User`, `WorkRequest` and `Client` are the resources.

### 2.5 Policies
A `Policy` is a set of declaration or rules that governs access to Resource. Policies is the one that will glue roles, resource, resource attribute, groups, user, user attribute, etc to provide a decision on access or authorization request on a Resource.

The below indicates, and example policy, where user is accessing a document belonging to a client. Then the policy would be defined as:
```
# By default, any kind of accesses to a service is denied.
default allow = false

# A rule that will allow access to document owned by a client, if all conditions are met
allow {
    # A variable to hold provided id of the client
    some clientId  

    # The Action on the resource, which is read in this case
    input.method = "GET"

    # The resource address, also contains the resource owning ClientId.
    # Direct qualified access to any document for the client is allowed.
    input.path = ["clients",clientId,"documents", _]
    
    # Roles that are allowed to access the client and document
    required_roles = {"app1.sys.allow.clients."+clientId,"app1.sys.allow.documents.read"}
    
    # For end user, ideally all the roles should be in the JWT token.
    isAllowedRolePresent(required_roles, jwt.payload.roles)
}

# A helper function that would compare required and provided roles, 
# and return `true` if all the required roles are present in the provided roles. 
isAllowedRolePresent(required_roles,provided_roles){
    # Convert provided roles to set
    provided_roles_set := { role| role = provided_role[_] }
    
    # Find whether the provided roles have all the required roles.
    # When all the required roles exist then the substraction result will be Zero. 
    count(permitted_roles - provided_roles_set) == 0
}

```
> **Note:** The example policy above is written in Rego language, which is used by OPA to write the policy files.

Policy files are similar to configuration files, written using `Rego`,  which sits outside of application code, and executed  to get authorization results when an authenticated user will try to access a resource.


## 3 Authorization Flows

### 3.1 JWT Creation Post (successful) Authentication
MSSO will be the system that will be used for authenticating an user. Post successful authentication the MSSO will pass on signed SAML token to the `Main Back End` (MBE) or `Admin Back End` (ABE). They will use the `authn` library to validate the SAML and create a JWT token. The JWT token should have all the information for correctly authorizing the user.

#### 3.1.1 Flow diagram

```mermaid
sequenceDiagram
autonumber
participant BRW as User/Browser
participant MSSO as MSSO
participant BE as Admin/Main Back End Service
participant AUTHZ as Authorization Service <br>(Authorization Control Plane)

BRW ->> MSSO: provides login credentials
MSSO -->> BRW: provides SAML on sucessful authentication
BRW  ->> BE: provides SAML
BE ->> AUTHZ: querries for user access cotrol data
AUTHZ -->> BE: provides user access control data
BE ->> BE: generates signed JWT token with user access control data
BE -->> BRW: provides signed JWT
```

### 3.2 Policy Evaluation during Authorization
Every call to access or update resource must the accompanied by JWT. If JWT is absent then the user should be considered as **Unauthenticated**.
The Policies will be drafted using OPA policy language, also known as *Rego*. The policies will be evaluated by the OPA service, which will be availble as a `npm` module or side-car service, in the MercerOS Platform.

> **Note:** When hosted on MercerOS OSS 2.0 platform, it will run as a side car to Main API, and thus call to the OPA engine will be local not remote. Side-car pattern is not available in OSS1. Side-car pattern will also OPA policies to use dynamic control data, and enable more complex functions.

OPA engine will expect the following as input data:
- JWT
- Resource URI
- Requested Action on resource

#### 3.2.1 Flow diagram

```mermaid
sequenceDiagram
autonumber
participant BRW as User/Browser
participant MA as Main API Server
participant OPA as Open Policy Agent Service (Autorization Data Plane)

BRW  ->> MA: seeks resource, with JWT
MA -->> MA: extract bussiness control data (optional)
MA ->> OPA: provides input + JWT + business control data for policy check
OPA -->> OPA: validates JWT (user authentication asserted)
OPA -->> OPA: executes input data against Policies
OPA -->> MA: responds with Policy result
MA -->> BRW: reponds with resource, when Authorized
```

### 3.3 Authorization Control Data
The authorization control data can be split into below categories:
- User Roles
- Access Policies

#### 3.3.1 User Roles and Permissions
MercerOS's **Authorization Service** will be responsible for mastering the user roles and groups. Application will provide UIs for users to populate the user details, assign or create appropriate Groups and Roles.

The Roles are expected to be populated in the Authorization service during the development phase of the application. Fine-grained roles be hardened and associated with an action on a resource.

The roles will be either mapped to existing Groups, (coarse-grained) Roles or new Groups/Roles.

> Note: Presently, Authorization Service does not allow Role heirarchy. This feature development is still pending. 

The APIs associated with the Authorization Service is available at:
https://bitbucket.org/oliverwymantechssg/ngpd-merceros-authorization-api/src/master/

#### 3.3.2 JWT (Javascript Web Token)
The MercerOS have reusable [Authentication component](https://bitbucket.org/oliverwymantechssg/ngpd-merceros-authentication-be-components/src/master/) that issues JWT post authentication.
The JWT token issued by the component need to include additional claim information. The expected schema of JWT Claim is mentioned in format of Json Schema and YAML
```yaml
JWT Claim Schema:
  type: object
  properties:
    iss:
      type: string
      required: true
      description: |
        The "iss" (issuer) claim identifies the principal that issued the
        JWT.  The processing of this claim is generally application specific.
        The "iss" value is a case-sensitive string containing a StringOrURI
        value.
        e.g. This could be something like `com.mercer.digital.auth`
    sub:
      type: string
      required: true
      description: |
        The "sub" (subject) claim identifies the principal that is the
        subject of the JWT.  The claims in a JWT are normally statements
        about the subject.  The subject value MUST either be scoped to be
        locally unique in the context of the issuer or be globally unique.
        The processing of this claim is generally application specific.  The
        "sub" value is a case-sensitive string containing a StringOrURI
        value.
        e.g This should hold the user global identifier provided by MSSO, on MercerOS platform.
    aud:
      type: array
      items:
        properties:
          audience:
            type: string
      description: |
        The "aud" (audience) claim identifies the recipients that the JWT is
        intended for.  Each principal intended to process the JWT MUST
        identify itself with a value in the audience claim.  If the principal
        processing the claim does not identify itself with a value in the
        "aud" claim when this claim is present, then the JWT MUST be
        rejected.  In the general case, the "aud" value is an array of case-
        sensitive strings, each containing a StringOrURI value.  In the
        special case when the JWT has one audience, the "aud" value MAY be a
        single case-sensitive string containing a StringOrURI value.  The
        interpretation of audience values is generally application specific.
        e.g. ["client.gbp.mercer.com"]
    exp:
      type: string
      required: true
      description: |
        The "exp" (expiration time) claim identifies the expiration time on
        or after which the JWT MUST NOT be accepted for processing.  The
        processing of the "exp" claim requires that the current date/time
        MUST be before the expiration date/time listed in the "exp" claim.
        Implementers MAY provide for some small leeway, usually no more than
        a few minutes, to account for clock skew.  Its value MUST be a number
        containing a NumericDate value.
    jti:
      type: string
      required: true
      description: |
        The "jti" (JWT ID) claim provides a unique identifier for the JWT.
        The identifier value MUST be assigned in a manner that ensures that
        there is a negligible probability that the same value will be
        accidentally assigned to a different data object; if the application
        uses multiple issuers, collisions MUST be prevented among values
        produced by different issuers as well.  The "jti" claim can be used
        to prevent the JWT from being replayed.  The "jti" value is a case-
        sensitive string. This can be used to invalidate a JWT or authenticated session.

    groups:
      type: array
      items:
        properties:
          group:
            type: string
            description: Group of the user extracted from the Authorization Service

    roles:
      type: array
      required: true
      description: |
        An aggregated list of roles associated with the user.
        A role may be assigned to a user or a group. Where the user is associated with more than one group,
        then the roles of all the groups will be merged to an unique list. Roles are all additive.
      items:
        properties:
          role:
            type: string
            description: Role of the user extracted from the Authorization Service

    username:
      type: string
      description: The email of the user as provided by MSSOs

    languages:
      type: array
      description: List of languages that the user would prefer
      items:
        properties:
          language:
            type: string
            description: The language preferred by the user

    globalprofileid:
      type: number
      required: true
      description: The unique id of the user profile in MSSO service. The `sub` should be same as this, for single tenant applications.

    isFederated:
      type: boolean
      description: Type of user account, if true then it's federated, otherwise - branded, as provided by MSSO.


```

> Note: The structure suggested in the fields are based on the [IETF 7797](https://tools.ietf.org/html/rfc7519).

An example JWT payload will look like :
```json
{
  "globalprofileid": 1611162824992,
  "correlationId": "0BliKNfAbSdmGuSzXmK6IQHI",
  "username": "ops@mmc.com",
  "isWhitelistLogin": true,
  "sub": 1611162824992,
  "firstName": "ops",
  "lastName": "MMC",
  "roles": [
    "gbp.sys.mmb_operations",
    "gbp.sys.allow.data.country.br",
    "gbp.sys.allow.data.gbpclient.br192413",
    "gbp.sys.allow.data.gbpclient.br34936",
    "gbp.sys.allow.data.gbpclient.br192702"
  ],
  "iat": 1615200651,
  "exp": 1615204251,
  "aud": "gbp.mercer.com",
  "iss": "msso.mercer.com"
}
```

A `JWT` can hold more fields as private fields in it, that should aid in evaluation of the policies. It will be the responsibility of the JWT creation module to ensure that all the required fields of successful policy evaluation are provided. Absence of an information in the JWT will result is policy evulation as `false`, thus denial of resource to requestor JWT subject.


#### 3.3.3 Flow Diagram
The below indicates at high level how the interaction between various components will be when an user is on boarded, or permissions associated with an user is changed.

###### 3.3.3.1 User onboarding
```mermaid
sequenceDiagram
autonumber
participant BRW as Admin/User Front End
participant BE as Admin/Main Back End Service
participant AUTHZ as Authorization Service <br> (Authorization Control Plane)
participant MA as Main API Server
participant OPA as Open Policy Agent Service (Autorization Data Plane)

BRW ->> BE: onboard user
BE ->> MA: onboard user
MA ->> OPA: validate policies
OPA ->> MA: policy evaluation result
MA ->> AUTHZ: register user
AUTHZ -->> MA: user registered sucessfully (with default role)
MA ->> MA: register user with app/business store
MA -->> BE: successfully registered user
BE -->> BRW: successfully registered user
```

###### 3.3.3.2 User Access Control Updates
```mermaid
sequenceDiagram
autonumber
participant BRW as Admin/User Front End
participant BE as Admin/Main Back End Service
participant AUTHZ as Authorization Service (Authorization Control Plane)
participant MA as Main API Server
participant OPA as Open Policy Agent Service (Autorization Data Plane)

BRW ->> BE: update user's Group/Role
BE ->> MA: update user's Group/Role
MA ->> OPA: validate policies
OPA ->> MA: policy evaluation result
MA ->> AUTHZ: update user's association with Group/Role
AUTHZ -->> MA: user updated successfully
MA -->> BE: user updated successfully
BE -->> BRW: user updated successfully
```
> Note: Token re-issue should be reactive. When BE service is called for JWT renewal, BE should check for the latest state from Authorization server and issue the JWT with revised user roles.

###### 3.3.3.3 Group and Role Management
```mermaid
sequenceDiagram
autonumber
participant BRW as Admin/User Front End
participant BE as Admin/Main Back End Service
participant AUTHZ as Authorization Service (Authorization Control Plane)
participant MA as Main API Server
participant OPA as Open Policy Agent Service (Autorization Data Plane)

BRW ->> BE: create/update/read Group/Role
BE ->> MA: create/update/read Group/Role
MA ->> OPA: validate policies
OPA ->> MA: policy evaluation result
MA ->> AUTHZ: create/update/read Group/Role
AUTHZ -->> MA: create/update/read Group/Role success
MA -->> BE: create/update/read Group/Role success
BE -->> BRW: create/update/read Group/Role success
```
> Note: Token re-issue should be reactive. When BE service is called for JWT renewal, BE should check for the latest state from Authorization server and issue the JWT with revised user roles.

#### 3.3.4 Access Policies
OPA engine will be responsible for evaluating the policies associated with the users. The policies will be available in rego file format, and will be created during the introduction of a feature in the GBP.

Where a policy is absent for a feature, the default behaviour will be **Deny** during policy evaluation.

The user context (i.e. JWT token) and control data must be passed the OPA engine for evaluation. By default, all the policies and data that OPA uses to make decisions are kept in-memory. OPA is designed to enable distributed policy enforcement.

All the policies associated with MercerOS hosted application will be managed in a git repository that's separate from application code repository. Thus ensuring that more suitable governance can be applied to updates on policies.

> **Note:** The release/deployment of the Policy to OPA can be done in various ways. Presently, on MercerOS OSS1 deployed application WASM based deployment is allowed. 

##### 3.3.4.1 Policy Publishing to OPA
```mermaid
graph LR

subgraph Accessing Policies
J(Jenkins) --read Policies--> BB(Bitbucket/Git Repository)
end

subgraph  Publishing Policies
J --publishes Policies --> OPA(WASM npm module)
end
```


##### 3.3.4.2 Policy Evaluation at OPA
```mermaid
graph LR

subgraph Requesting Policies Evaluation
MA(Main API Service) --policy evaluation--> OPA(OPA Service)
MA --provides--> input>"Input (with JWT)"]
MA --provides-->control>"Control Data (Conditional)"]
OPA --access--> Policies[(Policy Cache)]
OPA --access--> input
OPA --access--> control
end
```
